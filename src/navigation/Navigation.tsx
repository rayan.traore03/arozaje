import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import DrawerNavigator from '../components/drawer/DrawerNavigatorRoutes';
import AuthNavigator from '../components/auth/AuthNavigator';
import SplashScreen from '../screens/auth/SplashScreen';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


const Stack = createNativeStackNavigator();
class Navigation extends React.Component {
    render() {
        return (
            <NavigationContainer>
                {/* <DrawerNavigator /> */}
                {/* <AuthNavigator /> */}
                <Stack.Navigator initialRouteName='splash'>
                    <Stack.Screen name="splash" component={SplashScreen} options={{ headerShown: false }}/>
                    <Stack.Screen name="auth" component={AuthNavigator} options={{ headerShown: false }} />
                    <Stack.Screen name="drawer" component={DrawerNavigator} options={{ headerShown: false }} />
                </Stack.Navigator>

            </NavigationContainer>
        );
    }
}

export default Navigation;