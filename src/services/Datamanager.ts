import axios from 'axios';



const apiURL = "http://projectrt.duckdns.org:413";


class DataManager {




    private static instance: DataManager;

    private constructor() {}
    
    public static getInstance(): DataManager {
        if (!DataManager.instance) {
        DataManager.instance = new DataManager();
        }
        return DataManager.instance;
    }

    // public static getApiURI() : string | undefined {
    //     return apiURL;
    // }
    // public static async getPlants() : Promise<any> {
    //     const { data } = await axios.get(`${apiURL}/plants`);
    //     return data;
    // }
    
    public static async getUserData(id :string) : Promise<any> {
        const { data } = await axios.get(`${apiURL}/users/${id}`);
        return data;
    }

    public static async getPlantData(id :string) : Promise<any> {
        const { data } = await axios.get(`${apiURL}/plants/${id}`);
        return data;
    }


    public static async getPlantsData() : Promise<any> {
        const { data } = await axios.get(`${apiURL}/plants`);
        return data;
    }


    public static async getPlantsDataByOwner(id :string) : Promise<any> {
        const { data } = await axios.get(`${apiURL}/plants/owner/${id}`);
        return data;
    }

    public static async getPlantsDataByRenter(id :string) : Promise<any> {
        const { data } = await axios.get(`${apiURL}/plants/renter/${id}`);
        return data;
    }

    public static async getDiscussionData(id :string) : Promise<any> {
        const { data } = await axios.get(`${apiURL}/discussions/${id}`);
        return data;
    }

    public static async login(email :string, password :string) : Promise<any> {

        const { data } = await axios.post(`${apiURL}/users/login`, {
            email,
            password
        });
        return data;
    }

    public static async register(email :string, password :string, firstName :string, lastName :string, role: boolean, address: string) : Promise<any> {
            
            const { data } = await axios.post(`${apiURL}/users/register`, {
                email,
                password,
                firstName,
                lastName,
                role: role ? "expert" : "user",
                address: address
            });
            return data;
        }
    
    // public static async createPlant(specie :string, description :string, images :string[], mainImage :string, owner :string) : Promise<any> {
    //     const { data } = await axios.post(`${apiURL}/plants/`, {
    //         specie,
    //         description,
    //         images,
    //         mainImage,
    //         owner
    //     });
    //     return data;
    // } 

}


export default DataManager;
