import User from './user';
interface Plant {
    id: string;
    specie : string;
    description: string;
    images : string[];
    mainImage : string;
    owner: User;
    renter? : User ;
}

export default Plant;