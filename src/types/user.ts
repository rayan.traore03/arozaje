import Plante from "./plant";


interface Position {
    latitude: number;
    longitude: number;
}
 interface User {
    id : string;
    avatar? : string ;
    address : string;
    firstName : string;
    lastName : string;
    rate : number;
    role: "user" | "expert";
    position?: Position
}


export default User;
