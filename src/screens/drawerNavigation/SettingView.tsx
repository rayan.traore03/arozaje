import { StyleSheet, Text, View, FlatList, TouchableOpacity, Switch, Modal } from 'react-native';
import React, { useState } from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';

const DATA = [
  {
    id: '1',
    title: 'Notifications',
    icon: 'notifications',
    isEnabled: true,
  },
  // {
  //   id: '2',
  //   title: 'Language',
  //   icon: 'language',
  //   isEnabled: false,
  // },
  {
    id: '3',
    title: 'Help',
    icon: 'help-outline',
    isEnabled: true,
  },
  {
    id: '4',
    title: 'Déconnexion',
    icon: 'power-settings-new',
    isEnabled: true,
  },
];

export default function SettingView() {
  const navigation = useNavigation();
  const [items, setItems] = useState(DATA);
  //const [showLanguageModal, setShowLanguageModal] = useState(false);
  //const [selectedLanguage, setSelectedLanguage] = useState('');
  const [showHelpModal, setShowHelpModal] = useState(false);

  const renderItem = ({ item }) => {
    const handleToggleSwitch = () => {
      const newItems = items.map((i) => {
        if (i.id === item.id) {
          return { ...i, isEnabled: !i.isEnabled };
        }
        return i;
      });
      setItems(newItems);
    };

    // const handleLanguageSelection = (language) => {
    //   setSelectedLanguage(language);
    //   setShowLanguageModal(false);
    // };

    const handleHelpButtonPress = () => {
      setShowHelpModal(true);
    };
    const handleLogout = () => {

      AsyncStorage.clear();
      navigation.navigate('auth');

      // code pour déconnecter l'utilisateur
    };

    return (
      <TouchableOpacity style={styles.row} onPress={item.id === '1' ? handleToggleSwitch : undefined}>
        <View style={styles.iconContainer}>
          <MaterialIcons name={item.icon} size={30} />
        </View>
        <Text style={styles.label}>{item.title}</Text>
        {item.id === '1' && (
          <Switch value={item.isEnabled} onValueChange={handleToggleSwitch} />
        )}
        {/* {item.id === '2' && (
          <TouchableOpacity style={styles.languageButton} onPress={() => setShowLanguageModal(true)}>
            <Text style={styles.languageButtonText}>{selectedLanguage || 'Select Language'}</Text>
          </TouchableOpacity>
        )} */}
        {item.id === '3' && (
          <TouchableOpacity style={styles.helpButton} onPress={handleHelpButtonPress}>
            <Text style={styles.helpButtonText}>Help</Text>
          </TouchableOpacity>
        )}

        {item.id === '4' && (
        <TouchableOpacity style={styles.logoutButton} onPress={handleLogout}>
          <MaterialIcons name={item.icon} size={30} />
        </TouchableOpacity>
      )}
   

      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList data={items} renderItem={renderItem} keyExtractor={(item) => item.id} />

      {/* <Modal animationType="slide" visible={showLanguageModal}>
        <View style={styles.modalContainer}>
          <TouchableOpacity onPress={() => setShowLanguageModal(false)}>
            <Text style={styles.modalCloseButtonText}>Close</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleLanguageSelection('English')}>
            <Text style={styles.modalLanguageOption}>English</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleLanguageSelection('French')}>
            <Text style={styles.modalLanguageOption}>French</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => handleLanguageSelection('Spanish')}>
            <Text style={styles.modalLanguageOption}>Spanish</Text>
          </TouchableOpacity>
        </View>
      </Modal> */}
      <Modal animationType="slide" visible={showHelpModal}>
        <View style={styles.modalContainer}>
          
          <Text style={styles.modalHelpText}>Bienvenue sur notre application de jardinage ! Nous sommes là pour vous aider à garder vos plantes en bonne santé et à profiter de votre jardin au maximum.

Si vous avez des questions sur l'arrosage, la fertilisation, la taille ou toute autre chose liée à l'entretien de vos plantes, nous sommes là pour vous aider. Voici quelques conseils pour vous aider à démarrer :

Arrosez vos plantes régulièrement, en fonction des besoins spécifiques de chaque plante. Vérifiez toujours le sol pour vous assurer qu'il est humide avant d'arroser à nouveau.

Fertilisez vos plantes avec un engrais spécifique pour les plantes que vous cultivez. N'oubliez pas de suivre les instructions de l'engrais pour éviter de sur-fertiliser vos plantes.

Taillez vos plantes régulièrement pour maintenir leur forme et leur santé. La plupart des plantes ont besoin d'être taillées au moins une fois par an, souvent au printemps ou à l'automne.

Gardez un œil sur les ravageurs et les maladies. Si vous voyez des signes de dommages causés par des insectes ou des maladies, agissez rapidement pour éviter une infestation.

Si vous avez des questions spécifiques sur l'entretien de vos plantes, n'hésitez pas à nous contacter. Nous sommes là pour vous aider à garder vos plantes en bonne santé et à profiter de votre jardin au maximum !</Text>
<TouchableOpacity onPress={() => setShowHelpModal(false) }>
            <Text style={styles.modalCloseButtonText}>Fermer</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </View>
  );

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 16,
  },
  label: {
    flex: 1,
    fontSize: 18,
    marginLeft: 16,

  },
  switch: {
    marginLeft: 16,
  },
  languageButton: {
    marginLeft: 16,
  },
  languageButtonText: {
    fontSize: 18,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalCloseButtonText: {
    fontSize: 18,
    marginBottom: 16,
  },
  modalLanguageOption: {
    fontSize: 18,
    marginBottom: 16,
  },
  helpButton: {
    //backgroundColor: '#007aff',
    padding: 10,
    borderRadius: 5,
    marginLeft: 16,
  },
  helpButtonText: {
    color: '#000',
    fontSize: 18,
  },
  modalHelpText: {
    fontSize: 15,
    marginBottom: 16,
  paddingBottom: 16,
  paddingHorizontal: 16,
  },
  logoutButton: {
    marginLeft: 'auto',
    position: 'absolute',
    bottom: 0,
  },

});

 
