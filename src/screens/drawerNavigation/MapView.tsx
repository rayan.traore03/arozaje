import * as React from 'react';
import MapView, { Marker } from 'react-native-maps';
import { StyleSheet, View } from 'react-native';
import axios from 'axios';
import Plant from '../../types/plant';
import DataManager from '../../services/DataManager';

interface IMapViewState {
    plants: Plant[];
}

class MapARView extends React.Component<{}, IMapViewState> {

    constructor(props: {}) {
        super(props);
        this.state = {
            plants: [],
        };
    }

    // componentDidMount(): void {
        
    //     DataManager.getPlantsData()
    //         .then((plants) => {
    //             const newPlants = plants.map((plant: any) => {
    //                 const { id, specie, description, mainImage, images, owner, renter } = plant;
    //                 DataManager.getUserData(owner)
    //                     .then((user) => {
    //                         const { id, address, firstName, lastName, role, rate, position } = user;
    //                         const newOwner = { id, address, firstName, lastName, role, rate, position };
    //                     }

    //             });
    //         });
    // }

    render() {
        return (
            <View style={styles.container}>
            <MapView style={styles.map}>
              {this.state.plants.map((plant: Plant) => (
                <Marker
                  key={plant.id}
                  coordinate={{ latitude:  plant.owner.position ?
                    plant.owner.position.latitude : 0, longitude:  plant.owner.position ?  plant.owner.position.longitude : 0  }}
                  title={plant.owner.firstName}
                  description={plant.owner.address}
                />
              ))}
            </MapView>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    map: {
      width: '100%',
      height: '100%',
    },
  });


export default MapARView;