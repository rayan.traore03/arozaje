import * as React from 'react';
import { Text, View, StyleSheet, SafeAreaView } from 'react-native';
import TabMenuGarden from '../../components/drawer/garden/TabMenuGarden';

import UserHeader from '../../components/drawer/garden/UserHeader';
import User from '../../types/user';
import Plant from '../../types/plant';
import AsyncStorage from '@react-native-async-storage/async-storage';



const style = StyleSheet.create({

  
    container: {  
        // backgroundColor: MainStyle.view.backgroundColor,
        // justifyContent: 'center',
        // alignContent: 'center',
        // alignItems: 'center',
        
        // width: '90%',
        height: '100%',
        // flexDirection: 'row',
        paddingVertical: 5,
    }});


interface IGardenViewState {
    user: User;
    ourPlants: Plant[];
    keepPlants: Plant[];

}


class GardenView extends React.Component<{}, IGardenViewState> {

    constructor(props: {}) {
        super(props);
        this.state = {
            user: {
                id: "",
                address : "",
                firstName : "",
                lastName : "",
                role : "user",
                rate : 0,
            },
            ourPlants: [],
            keepPlants: []
        }
    }

    componentDidMount(): void {
        AsyncStorage.getItem('user')
            .then((value) => {
                if (value !== null) {

        const { id, address, firstName, lastName, role, rate } = JSON.parse(value);
        this.setState({user: 
                        { 
                          id, 
                          address, 
                          firstName, 
                          lastName, 
                          role, 
                          rate
                        }});
                      }   
            });


    }


    render() {
        return (
          <SafeAreaView style={style.container}>
            <UserHeader 
                firstName={this.state.user.firstName}
                lastName={this.state.user.lastName}
                rate={this.state.user.rate}
                role={this.state.user.role}
            />
            <TabMenuGarden />
          </SafeAreaView>
        );
      }

    
}

export default GardenView;