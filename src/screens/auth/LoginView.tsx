import * as  React from 'react';

import AsyncStorage from '@react-native-async-storage/async-storage';


import { 
    StyleSheet,
    TextInput,
    View,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    KeyboardAvoidingView,
    } from 'react-native';
import Color from '../../constants/Color';
import DataManager from '../../services/DataManager';


    

interface TSingInViewProps {
    navigation: any;
    
}

interface ISingInViewState {
    email: string;
    password: string;
    error : boolean;
}

class LoginView extends React.Component< TSingInViewProps, ISingInViewState > {


    constructor(props: TSingInViewProps) {
        super(props);
        this.handleClickOnLogin = this.handleClickOnLogin.bind(this);
        this.state = {
            email: '',
            password: '',
            error: false,
        
        }
    }

    

    handleClickOnLogin = () => {
        const { email, password } = this.state;
        const { navigation } = this.props;
        DataManager.login(email, password)
            .then((response) => {
                if (!!response.user.id) {
                    AsyncStorage.setItem('user', JSON.stringify(response.user));
                    console.log(response);
                    navigation.replace("splash");
                }
                console.log(response);
            })
            .catch((error) => {
                this.setState({ error: true });

                console.log(error);
            }
            );
    }



    handleEmailChange = (email: string) => {
        this.setState({ email });
      }
    
      handlePasswordChange = (password: string) => {
        this.setState({ password });
      }
    // handlePress() {
    //     const { navigation } = this.props;
    //     navigation.navigate("message");
    // }

    render() {
        const styles = StyleSheet.create({
            body : {
                // flex: 1,
                justifyContent: 'center'
            },
    
            imageContainer : {
                paddingTop: 50,
                alignItems: 'center',
                // justifyContent: 'center',
                width: "100%",
                justifyContent: 'center',
                // height: 300,
                
            },
            inputContainer : {
                alignItems: 'center',
                // justifyContent: 'center',
                // width: '100%',s
                // height: 300,
                
    
            },
    
            image : {
                width: '100%',
                
                height: 80,
                resizeMode: 'contain',
                // margin: 30
                // backgroundColor: Color.green.midnightGreen,
            },
            section : {
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                margin: 15,
                // backgroundColor: Color.green.midnightGreen,
            },
            loginButton : {
                backgroundColor: Color.green.midnightGreen ,
                borderRadius: 10,
                padding: 10,
                alignItems: 'center',
                justifyContent: 'center',
                margin: 15,
                height: 60,
                width: 300,
                
            },
            input : {
                borderColor:  this.state.error ? "red" : Color.green.midnightGreen,
                borderWidth: 1,
                height: 60,
                width: 300,
                borderRadius: 10,
                padding: 10,
                alignItems: 'center',
                justifyContent: 'center',
                // margin: 15,
            },
           
            loginTitle : {
                fontSize: 60,
                fontWeight: 'bold',
                color: Color.green.midnightGreen,
                // margin: 30,
            }
            
        });
        return (
            <View style={styles.body}>
                <ScrollView
                        keyboardShouldPersistTaps="handled"
                >
                    <KeyboardAvoidingView behavior="padding" enabled>
                        <View>

                            <View
                                style={styles.imageContainer}
                            >
                                <Image source={require('../../assets/image/logoFeuille.png')} 
                                    style={styles.image}
                                />
                            <View style={
                                    {
                                        width: 300,
                                    }
                                }>
                            
                            <Text style={styles.loginTitle}>login</Text>
                                <Text style={{
                                    fontSize: 20,

                                }}>Connexion à votre compte</Text>
                            </View>
                            </View>
                            <View style={styles.inputContainer}>
                                <View style={styles.section}>
                                    <TextInput style={styles.input}
                                    autoCapitalize="none"
                                    keyboardType='email-address'
                                    
                                    value={this.state.email}
                                    onChangeText={this.handleEmailChange}
                                    
                                    placeholder="Email" />
                                </View>
                                <View style={styles.section}>
                                    <TextInput style={styles.input} 
                                    value={this.state.password}
                                    secureTextEntry={true}
                                    onChangeText={this.handlePasswordChange}
                                    
                                    placeholder="Password" />
                                </View>
                                {/* <TouchableOpacity>
                                    <Text>Se connecter</Text>
                                </TouchableOpacity> */}
                                <TouchableOpacity style={ {
                                    alignItems: 'flex-start',
                                    width: 300,
                                    marginBottom : 5,
                                } }>
                                    <Text>Mot de passe oublié ?</Text>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={this.handleClickOnLogin}
                                >

                                    <View
                                        style={this.state.error ? {
                                            alignItems: 'center',
                                        } : {display: 'none'}}
                                    >
                                        <Text
                                            style={{
                                                color: 'red',
                                                fontSize: 20,
                                                fontWeight: 'bold',
                                            }}
                                        >Email ou mots de passe incorrect</Text>
                                    </View>

                                    <View style={styles.loginButton}>
                                        <Text style={{color: Color.white}}
                                        
                                        
                                        >Se connecter</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate('singIn');
                                    }}

                                >
                                    <Text>Pas encore de compte ?<Text style={{color: Color.green.midnightGreen}}>S'inscrire</Text></Text> 
                                </TouchableOpacity>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </View>
        );
    }
}

export default LoginView;

