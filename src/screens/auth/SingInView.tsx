import AsyncStorage from '@react-native-async-storage/async-storage';
import * as React from 'react';


import { 
    StyleSheet,
    TextInput,
    View,
    Text,
    ScrollView,
    Image,
    Button,
    Keyboard,
    TouchableOpacity,
    KeyboardAvoidingView,
    } from 'react-native';
import { Switch } from 'react-native-paper';
import Color from '../../constants/Color';
import DataManager from '../../services/DataManager';


    const styles = StyleSheet.create({
        body : {
            // flex: 1,
            justifyContent: 'center'
        },

        imageContainer : {
            paddingTop: 50,
            alignItems: 'center',
            // justifyContent: 'center',
            width: "100%",
            justifyContent: 'center',
            // height: 300,
            
        },
        inputContainer : {
            alignItems: 'center',
            // justifyContent: 'center',
            // width: '100%',
            // height: 300,
            

        },

        image : {
            width: '100%',
            
            height: 80,
            resizeMode: 'contain',
            // margin: 30
            // backgroundColor: Color.green.midnightGreen,
        },
        section : {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            margin: 15,
            // backgroundColor: Color.green.midnightGreen,
        },
        loginButton : {
            backgroundColor: Color.green.midnightGreen ,
            borderRadius: 10,
            padding: 10,
            alignItems: 'center',
            justifyContent: 'center',
            margin: 15,
            height: 60,
            width: 300,
            
        },
        
        input : {
            borderColor: Color.green.midnightGreen,
            borderWidth: 1,
            height: 60,
            width: 300,
            borderRadius: 10,
            padding: 10,
            alignItems: 'center',
            justifyContent: 'center',
        },
        loginTitle : {
            fontSize: 60,
            fontWeight: 'bold',
            color: Color.green.midnightGreen,
            // margin: 30,
        }
        
    });

interface ISingInViewProps {
    navigation: any;
    
}

interface ISingInViewState {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    swith : boolean;
    address : string;
}
class SingInView extends React.Component< ISingInViewProps, ISingInViewState> {


    constructor(props: ISingInViewProps) {
        super(props);
        this.handlePress = this.handlePress.bind(this);
        this.state = {
            email: '',
            password: '',
            firstName: '',
            lastName: '',
            swith: false,
            address: '',
        };
        
    }

    handlePress() {
        const { navigation } = this.props;
        DataManager.register(this.state.email, this.state.password, this.state.firstName, this.state.lastName, this.state.swith, this.state.address)
        .then((response) => {
            if (response.status === 200) {
                    navigation.navigate("login");
                }  
        }   )        
    }

    handleEmailChange = (email: string) => {
        this.setState({ email });
    };

    handlePasswordChange = (password: string) => {
        this.setState({ password });
    };


    handleFistNameChange = (firstName: string) => {
        this.setState({ firstName });
    };

    handleLastNameChange = (lastName: string) => {
        this.setState({ lastName });
    };

    handleSwithChange = () => {
        this.setState({ swith: !this.state.swith });
    };

    handleAddressChange = (address: string) => {
        this.setState({ address });
    };



    render() {
        return (
            <View style={styles.body}>
                <ScrollView
                        keyboardShouldPersistTaps="handled"
                >
                    <KeyboardAvoidingView behavior="padding" enabled>
                        <View>

                            <View
                                style={styles.imageContainer}
                            >
                                <Image source={require('../../assets/image/logoFeuille.png')} 
                                    style={styles.image}
                                />
                            <View style={
                                    {
                                        width: 300,
                                    }
                                }>
                               
                            <Text style={styles.loginTitle}>Bienvenue</Text>
                                <Text style={{
                                    fontSize: 20,
                                }}>Inscriver vous </Text>
                            </View>
                            </View>
                            <View style={styles.inputContainer}>
                                <View style={styles.section}>
                                    <TextInput 
                                    value={this.state.email}
                                    onChangeText={this.handleEmailChange}
                                    style={styles.input} placeholder="Email" />
                                </View>
                                <View style={styles.section}>
                                    <TextInput 
                                    value={this.state.password}
                                    onChangeText={this.handlePasswordChange}

                                    style={styles.input} placeholder="Nom" />
                                </View>
                                <View style={styles.section}>
                                    <TextInput 
                                    value={this.state.firstName}
                                    onChangeText={this.handleFistNameChange}

                                    style={styles.input} placeholder="Prenom" />
                                </View>
                                
                                <View style={styles.section}>
                                    <TextInput 
                                    value={this.state.lastName}
                                    onChangeText={this.handleLastNameChange}

                                    style={styles.input} placeholder="mots de passe" />
                                </View>
                                <View style={styles.section}>
                                    <TextInput 
                                    value={this.state.address}
                                    onChangeText={this.handleAddressChange}

                                    style={styles.input} placeholder="adresse" />
                                </View>
                                
                                <View style={styles.section}>
                                    <Switch
                                        
                                        value={this.state.swith}
                                        onValueChange={this.handleSwithChange}
                                        color={Color.green.midnightGreen}

                                    />
                                </View>
                            
                                <TouchableOpacity
                                    onPress={this.handlePress}
                                >
                                    <View style={styles.loginButton}>
                                        <Text style={{color: Color.white}}>S'inscrire</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate('login');
                                    }}

                                >
                                    <Text>Vous avez déja un compte ?<Text style={{color: Color.green.midnightGreen}}>Se connecter</Text></Text> 
                                </TouchableOpacity>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </View>
        );
    }
}

export default SingInView;
