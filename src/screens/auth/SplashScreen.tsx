import * as React from 'react';

import { View, Text, StyleSheet, ActivityIndicator, Image } from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
});

interface State { 
    animating: boolean;
}

interface Props {
    navigation: any;
}

class SplashScreen extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            animating: true,
        };
    }


    componentDidMount() {
        
        setTimeout(() => {
            this.setState({animating: false});
            AsyncStorage.getItem('user')
                .then((value) => {
                    
                    value === null ?  this.props.navigation.replace('auth') : this.props.navigation.replace('drawer');
                });
        }, 2000);
    }


    

  render() {
    return (
      <View style={styles.container}>
        <Image 
            source={require('../../assets/image/logo.png')}
            style={{
              resizeMode: 'contain',
              width: "90%"
            }}
        />
        <ActivityIndicator size="large" />
      </View>
    );
  }
}

export default SplashScreen;