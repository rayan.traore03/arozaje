import * as React from 'react';
import User from '../../../types/user';
import { Text, View, StyleSheet, Image  } from 'react-native';
// import TabBarIcon from '../../../constants/Color';
import Color from '../../../constants/Color';
import TabBarIcon from '../../../utils/TabIcon';

const style = StyleSheet.create({
  container: {
    backgroundColor: Color.white,
    width: '95%',
    flexDirection: 'row',
    padding: 5,
    borderRadius: 20,
    alignSelf: 'center',
    marginVertical: 15,
  },
  name : {
    fontSize: 20,
    fontWeight: 'bold',
    margin: 10,
  },
  rate : {
    flexDirection: 'row',
    // justifyContent: 'center',
    // alignItems: 'center',
    // margin: 10,
    marginHorizontal: 10,

  }
});

interface IUserHeaderProps {
  firstName: string;
  lastName: string;
  rate: number;
  role: "user" | "expert";
}


class UserHeader extends React.Component<IUserHeaderProps> {

  constructor(props: IUserHeaderProps) {
    super(props);
  }

  render() {
    return (
      <View style={style.container}>
        
          <Image source={require('../../../assets/image/profile.jpg')} 
            style={{width: 100, height: 100, borderRadius: 20, margin: 10}}
          />
          <View>
          <Text style={style.name}>{this.props.firstName+" "+this.props.lastName}</Text>
          <View style={style.rate}>
            {
              [...Array(5)].map((e, i) => {
                return (
                  <TabBarIcon key={i} name="star" color={i < this.props.rate ? "gold" : Color.gray}  />
                );
              })

            }
          </View>
          <View>
            <Text style={{fontSize: 20, margin: 10, marginTop: 20}}>{this.props.role === "expert" ? "botaniste" : "utilsateur"}</Text>
          </View>
          </View>
      
      </View>
    );
  }
}


export default UserHeader;