import * as React from 'react';
// import uuid from 'react-native-uuid';
import { Text, View, StyleSheet, ScrollView, FlatList  } from 'react-native';
import DataManager from '../../../../services/DataManager';
import Plant from '../../../../types/plant';
import UniquePlantItem from './UniquePlantItem';
import User from '../../../../types/user';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Navigation from '../../../../navigation/Navigation';


const style = StyleSheet.create({

    columnWrapper: {
        justifyContent: 'space-around',
        marginBottom: 10,
      },

  containerOfPlant: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%',
      paddingTop: 10,
      justifyContent: 'space-around',
  },
});

interface IOurPlantsState {
    plants : Plant[]
    user: User
}


interface  IOurPlantsProps {
    navigation: any
}


class OurPlants extends React.Component< IOurPlantsProps, IOurPlantsState> {

    constructor(props: IOurPlantsProps) {
        super(props);
        this.state = {
            plants: [],
            user : {
                id: "",
                address : "",
                firstName : "",
                lastName : "",
                role : "user",
                rate : 0,

            }
        }
    }

    componentDidMount() {
        AsyncStorage.getItem('user').then((value) => {
            if (value !== null) {
                const { id, address, firstName, lastName, role, rate } = JSON.parse(value);
                this.setState({user:
                        {
                            id,
                            address,
                            firstName,
                            lastName,
                            role,
                            rate,
                        }
                });
            }
        });
        DataManager.getPlantsDataByOwner(this.state.user.id).then((plants) => {
            var renterUser: User | undefined;
            const newPlants = plants.map((plant: any) => {
                const { plant_id, specie, renter, lmainImage, images, description } = plant;

                DataManager.getUserData(renter).then((user) => {
                    if (user !== undefined) {
                        renterUser = user;
                    } else {
                        renterUser = undefined;
                    }
                }).catch((error) => {
                    console.log(error);
                });


                if (renterUser !== undefined) {
                    return {
                        id: plant_id,
                        specie: specie,
                        renter: renterUser,
                        mainImage: lmainImage,
                        images: images,
                        description: description,
                    }
                } else {
                    return {
                        id: plant_id,
                        specie: specie,
    
                        mainImage: lmainImage,
                        images: images,
                        description: description,
                    }
                }
                    
            });
                this.setState({ plants: newPlants });
        });

    }

        


render() {
    
    return (

   
        <FlatList
            data={this.state.plants}
            renderItem={({ item }) => (
                <UniquePlantItem
                    plant={item}
                    navigation={this.props.navigation}
                />
            )}
            columnWrapperStyle={style.columnWrapper}
            numColumns={2}
            keyExtractor={(item) => item.id}
            
            
        />



    );

}

}

export default OurPlants;