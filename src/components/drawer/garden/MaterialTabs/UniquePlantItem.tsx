import * as React from 'react';

import { Text, View, Image, StyleSheet, Pressable } from 'react-native';
import Color from '../../../../constants/Color';
import Plant from '../../../../types/plant';
import KeepPlantModal from "../../modal/KeepPlantModal"


const style = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    // width: '50%',
    backgroundColor: Color.white,
    padding: 4,
    borderRadius: 15,
    alignItems: 'center',
  },
  img : {
    borderRadius: 15,
    width: 180,
    height: 180,
  }
});
interface IUniquePlantItemProps {
  plant : Plant
  navigation: any;
}

class UniquePlantItem extends React.Component<IUniquePlantItemProps> {
  constructor(props: IUniquePlantItemProps) {
    super(props);
    this.handlePress = this.handlePress.bind(this);
  }

  handlePress() {
    const { navigation } = this.props;
    navigation.navigate('KeepPlantModal', { plant: this.props.plant });
}



    render () {
      
    return (
      <View style={style.container}>
        <Pressable
          onPress={this.handlePress }
        >
          <Image style={style.img} source={require("../../../../assets/test/imagePlant5.png")} />
          <Text >{this.props.plant.specie}</Text>
        </Pressable>
      </View>
    );
    }
}

export default UniquePlantItem;