import * as React from 'react';

import { Text, View, StyleSheet, ScrollView  } from 'react-native';
import UniquePlantItem from './UniquePlantItem';
import Plant from '../../../../types/plant';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DataManager from '../../../../services/DataManager';
import User from '../../../../types/user';

const style = StyleSheet.create({
  containerOfPlant: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%',
      paddingTop: 10,
      justifyContent: 'space-around'
    }});



interface IKeepPlantsState {

    plants : Plant[]
    user: User

}
class KeepPlants extends React.Component<{}, IKeepPlantsState>  {


  constructor(props: {}) {
    super(props);
    this.state = {
      plants: [],
      user : {
        id: "",
        address : "",
        firstName : "",

        lastName : "",
        role : "user",
        rate : 0,

    }
  }

  }
//   componentDidMount() {
//     AsyncStorage.getItem('user').then((value) => {
//         if (value !== null) {
//             const { id, address, firstName, lastName, role, rate } = JSON.parse(value);
//             this.setState({user:
//                     {
//                         id,
//                         address,
//                         firstName,
//                         lastName,
//                         role,
//                         rate,
//                     }
//             });
//         }
//     });
//     DataManager.getPlantsDataByRenter(this.state.user.id).then((plants) => {
//         var ownerUser: User | undefined;
//         const newPlants = plants.map((plant: any) => {
//             const { plant_id, specie, renter, lmainImage, images, description } = plant;

//             DataManager.getUserData(renter).then((user) => {
//                 if (user !== undefined) {
//                     ownerUser = user;
//                 } else {
//                     ownerUser = undefined;
//                 }
//             }).catch((error) => {
//                 console.log(error);
//             });


//             if (ownerUser !== undefined) {
//                 return {
//                     id: plant_id,
//                     specie: specie,
//                     owner: ownerUser,
//                     mainImage: lmainImage,
//                     renter: this.state.user,
//                     images: images,
//                     description: description,

//                 }
//             } else {
//                 return 
//             }
                
//         });
//             this.setState({ plants: newPlants });
//     });

// }




  chunk = (arr: any, size: number) =>
  Array.from({ length: Math.ceil(arr.length / size) }, (_, i) =>
      arr.slice(i * size, i * size + size)
);



  render() {
    
    const chunkedData = this.chunk(this.state.plants, 2);
    return (

    <ScrollView>
                <View style={style.containerOfPlant}>
                {chunkedData.map((chunk, index) => (
                    <View key={index} style={{ flexDirection: "row" }}>
                        {chunk.map((plant: Plant) => (
                            <UniquePlantItem key={plant.id} plant={plant}  navigation/>
                            // <UniquePlantItem key={plant.id}  navigation={undefined} />
                        ))}
                    </View>
                    ))}
                </View>
    </ScrollView>


    );
                        }
}

export default KeepPlants;