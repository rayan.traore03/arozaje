import * as React from 'react';
import { NavigationContainer,  } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import LoginView from '../../screens/auth/LoginView';
import SingInView from '../../screens/auth/SingInView';
import TestView from '../../screens/auth/TestView';



const Stack = createNativeStackNavigator();

class AuthNavigator extends React.Component {


    render() {
        return (
        
            <Stack.Navigator>
                <Stack.Screen name="login" component={LoginView} 

                    options={{ headerShown: false }}
                />
                <Stack.Screen name="singIn" component={SingInView}
                    options={{ headerShown: false }}
                />
                {/* <Stack.Screen name="test" component={TestView} options={{ headerShown: false }} /> */}
            </Stack.Navigator>
        
        );
    }
}

export default AuthNavigator;
